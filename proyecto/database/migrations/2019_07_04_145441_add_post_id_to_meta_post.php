<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostIdToMetaPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_post', function (Blueprint $table) {
          $table->unsignedInteger('post_id');
          $table->foreign('post_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_post', function (Blueprint $table) {
          $table->dropForeign(['post_id']);
          $table->dropColumn('post_id');
        });
    }
}
